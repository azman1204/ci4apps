<?php

namespace App\Controllers;
// import class
use App\Models\Product;
use App\Controllers\BaseController;

class ProductController extends BaseController {
    public function index() {
        $product = new Product(); // instance of Product
        // findAll() = select * from product
        $rows = $product->findAll(); // return array of Products
        //dd($rows);
        //return view('product/index',['rows' => $rows]);
        return view('product/index', compact('rows')); // Views/product/index.php
    }

    // show form product to insert
    function create() {
        return view('product/create');
    }

    public function insert() {
        $product = model(Product::class);
        // $data = [
        //     'name' => 'Kuih baulu',
        //     'description' => 'Kuih baulu ikan',
        //     'price' => 10
        // ];
        $data = $_POST;
        $product->insert($data);
        // redirect
        return redirect()->to('product/index');
    }

    // display edit of a record / product
    public function show($id) {
        $product = new Product();
        $row = $product->find($id); // return an object (Product)
        //echo $row->name;
        return view('product/edit', compact('row'));
    }

    public function update() {
        // kalau guna full path, x perlu use (import)
        $product = new \App\Models\Product();
        $id = $_POST['id'];
        // $data = [
        //     'name' => 'Kuih Kria'
        // ];
        $data = $_POST;
        $product->update($id, $data);
        return redirect()->to('product/index');
    }

    public function delete($id) {
        $product = new Product();
        $product->delete($id);
        return redirect()->to('product/index');
    }
}
