<?php

namespace App\Controllers;

class Home extends BaseController
{
    public function index()
    {
        // $this->load->view('welcome_message'); // ci 3
        return view('welcome_message');
    }
}
