<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?= $siteName ?></title>
</head>
<body>
    <h1>Welcome to <?= $siteName ?></h1>
    <p>
        Purveyor of fine
        <?php
        foreach($categories as $cat) {
            echo $cat . ',';
        }
        ?>
    </p>
</body>
</html>