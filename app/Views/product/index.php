<a href="<?= url_to('product/create') ?>">Add Product</a>

<table border="1">
    <tr>
        <td>No</td>
        <td>Name</td>
        <td>Description</td>
        <td>Price</td>
        <td>Action</td>
    </tr>
    <?php 
    $no = 1;
    foreach ($rows as $row): ?>
    <tr>
        <td> <?= $no++ ?> </td>
        <td> <?= $row->name ?> </td>
        <td> <?= $row->description ?> </td>
        <td> <?= $row->price ?> </td>
        <td>
            <a href="<?= base_url("product/delete/". $row->id) ?>">Delete</a>
            <a href="<?= base_url("product/details/". $row->id) ?>">Edit</a>
        </td>
    </tr>
    <?php endforeach; ?>
</table>